import random

filename = "input/c_memorable_moments.txt"
out_filename = f"{filename}_out"


def score(t1, t2):
    t1 = set(t1)
    t2 = set(t2)
    return min(len(t1.difference(t2)), len(t2.difference(t1)), len(t2.intersection(t1)))


vertical, vertical_pairs, horizontal = [], [], dict()

data = []


def horizontal_candidate(tags):
    if len(horizontal) == 0:
        return None, None, None
    start_ind = list(horizontal.keys())[0]
    res = horizontal[start_ind]
    best_score = score(tags, res['tags'])
    ind = start_ind
    for ind, t in horizontal.items():
        if ind != start_ind:
            s = score(t["tags"], tags)
            if s > best_score:
                best_score = s
                res = t
    # print(ind, res["ind"])
    return horizontal[ind], best_score, ind


def vertical_candidates(tags):
    global vertical_pairs
    if len(vertical_pairs) == 0:
        return None, None
    res = vertical_pairs[0]
    best_score = score(tags, res['tags'])
    for vv in vertical_pairs:
        s = score(vv["tags"], tags)
        if s > best_score:
            best_score = s
            res = vv

    new = list()
    cur_ids = res["ids"]
    for ii, v in enumerate(vertical_pairs):
        ids = v["ids"]
        if not (ids[0] == cur_ids[0] or ids[0] == cur_ids[1] or ids[1] == cur_ids[0] or ids[1] == cur_ids[1]):
            new.append(v)
    vertical_pairs = new

    return res, best_score


def add_line(orientation, tags, i):
    it = {
        "orientation": orientation,
        "tags": tags,
        "ind": i
    }

    if orientation == "V":
        v_ind = len(vertical)
        for ii, v in enumerate(vertical):
            vertical_pairs.append({
                "ids": (ii, v_ind),
                "tags": list(set(it["tags"]).union(v["tags"]))
            })
    if orientation == "V":
        vertical.append(it)
    else:
        horizontal[len(horizontal)] = it


with open(filename) as f:
    n = int(f.readline())
    for i in range(n):
        line = f.readline()
        o, _, tags = line.split(" ", 2)
        add_line(o, tags.split(" "), i)

ind = random.randint(0, len(horizontal) - 1)
prev = horizontal[ind]["tags"]
res = [horizontal[ind]["ind"]]
horizontal.pop(ind)


cnt = len(horizontal) + int(len(vertical) / 2)
score_acc = 0
for i in range(cnt):
    v_c, v_s = vertical_candidates(prev)
    h_c, h_s, h_ind = horizontal_candidate(prev)
    if v_s is None and h_s is None:
        break
    if v_s is None or (h_s is not None and v_s < h_s):
        prev = h_c["tags"]
        res.append(h_c["ind"])
        horizontal.pop(h_ind)
        score_acc += h_s or 0
    else:
        ids = v_c["ids"]
        try:
            v_c1, v_c2 = vertical[ids[0]], vertical[ids[1]]
        except:
            raise
        prev = v_c["tags"]
        res.append((v_c1["ind"], v_c2["ind"]))
        score_acc += v_s or 0

        # vertical.pop(ids[0])
        # vertical.pop(ids[1])
print(f"score {score_acc}")

with open(out_filename, "w") as of:
    of.write(f"{len(res)}\n")
    of.write("\n".join([str(i if isinstance(i, int) else " ".join(map(str, i))) for i in res]))
